<?php

require_once 'protectoptionvalues.civix.php';
// phpcs:disable
use CRM_Protectoptionvalues_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function protectoptionvalues_civicrm_config(&$config) {
  _protectoptionvalues_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function protectoptionvalues_civicrm_xmlMenu(&$files) {
  _protectoptionvalues_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function protectoptionvalues_civicrm_install() {
  _protectoptionvalues_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function protectoptionvalues_civicrm_postInstall() {
  _protectoptionvalues_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function protectoptionvalues_civicrm_uninstall() {
  _protectoptionvalues_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function protectoptionvalues_civicrm_enable() {
  _protectoptionvalues_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function protectoptionvalues_civicrm_disable() {
  _protectoptionvalues_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function protectoptionvalues_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _protectoptionvalues_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function protectoptionvalues_civicrm_managed(&$entities) {
  _protectoptionvalues_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function protectoptionvalues_civicrm_caseTypes(&$caseTypes) {
  _protectoptionvalues_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function protectoptionvalues_civicrm_angularModules(&$angularModules) {
  _protectoptionvalues_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function protectoptionvalues_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _protectoptionvalues_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function protectoptionvalues_civicrm_entityTypes(&$entityTypes) {
  _protectoptionvalues_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_themes().
 */
function protectoptionvalues_civicrm_themes(&$themes) {
  _protectoptionvalues_civix_civicrm_themes($themes);
}

/**
 * Implements hook_civicrm_permission().
 */
function protectoptionvalues_civicrm_permission(&$permissions) {
  $permissions['protectoptionvalues'] = E::ts('CiviCRM: administer reserved option values');
}

/**
 * Implements hook_civicrm_preProcess().
 */
function protectoptionvalues_civicrm_preProcess($formName, &$form) {
  if ($formName == 'CRM_Admin_Form_Options' || $formName == 'CRM_Custom_Form_Option') {
    // If they have the permission, don't do anything.
    if (CRM_Core_Permission::check('protectoptionvalues')) {
      return;
    }

    // On adding a new value, will only have the group id

    $option_value_id = empty($form->_id) ? NULL : $form->_id;
    $option_group_id = empty($form->_gid) ? NULL : $form->_gid;

    if ($formName == 'CRM_Custom_Form_Option') {
      // need to get from url since the values we want aren't public
      $option_value_id = CRM_Utils_Request::retrieve('id', 'Positive');
      if (empty($option_value_id)) {
        // need to get the field id, then from there get the option group id
        $field_id = CRM_Utils_Request::retrieve('fid', 'Positive');
        if (!empty($field_id)) {
          $custom_field = \Civi\Api4\CustomField::get()->addWhere('id', '=', $field_id)->execute()->first();
          $option_group_id = $custom_field['option_group_id'] ?? NULL;
        }
      }
    }

    $restrict = FALSE;
    // If it's reserved then restrict, otherwise look up the option group and
    // if that's reserved then restrict.
    if (!empty($option_value_id)) {
      $optval = \Civi\Api4\OptionValue::get()->addWhere('id', '=', $option_value_id)->execute()->first();
      $restrict = (($optval['is_reserved'] ?? 1) == 1);
    }
    // get option group id from the form or else the option value
    $option_group_id = empty($option_group_id)
      ? (empty($optval['option_group_id']) ? NULL : $optval['option_group_id'])
      : $option_group_id;
    if (!$restrict && !empty($option_group_id)) {
      $optgrp = \Civi\Api4\OptionGroup::get()->addWhere('id', '=', $option_group_id)->execute()->first();
      $restrict = (($optgrp['is_reserved'] ?? 1) == 1);
    }
    if ($restrict) {
      CRM_Core_Error::statusBounce(E::ts('These option choices are tied to either report functionality or automated processing. They can not be changed.'));
    }
  }
}

/**
 * Implements hook_civicrm_links().
 */
function protectoptionvalues_civicrm_links($op, $objectName, $objectId, &$links, &$mask, &$values) {
  // If they have the permission, don't do anything.
  if (CRM_Core_Permission::check('protectoptionvalues')) {
    return;
  }
  if (($op == 'optionValue.row.actions' && $objectName == 'optionValue') || ($op == 'customOption.row.actions' && $objectName == 'customOption')) {
    // If it's reserved then restrict, otherwise look up the option group and
    // if that's reserved then restrict.
    $restrict = FALSE;
    $optval = \Civi\Api4\OptionValue::get()->addWhere('id', '=', $objectId)->execute()->first();
    $restrict = (($optval['is_reserved'] ?? 1) == 1);
    if (!$restrict && !empty($optval['option_group_id'])) {
      $optgrp = \Civi\Api4\OptionGroup::get()->addWhere('id', '=', $optval['option_group_id'])->execute()->first();
      $restrict = (($optgrp['is_reserved'] ?? 1) == 1);
    }
    if ($restrict) {
      $links = [];
    }
  }
}
