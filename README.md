# protectoptionvalues

**This is a work in progress. Use on a live site at own risk.**

Adds a permission to prevent administrative actions on specified option values.

A use case is you've been asked to produce some functionality, e.g. automation based on drop-down choices or for some report, where you don't want the choices messed with. For example a feature that emails the boss when somebody selects "High" as the option choice. Then, somebody comes along and deletes your option values or changes the meaning of the option value so that people start to use it differently. Then three months later the boss wonders why they haven't received any alert emails, or the boss is a new boss that doesn't even know they're supposed to get those emails so the problem goes unnoticed.

You can restrict admin functions as a whole, but that's not granular enough in an org where they need/want admin functions in general.

The extension is licensed under [MIT](LICENSE.txt).

## Requirements

* PHP v7.2+
* CiviCRM 5.31+ (might work on earlier)

## Installation

Learn more about installing CiviCRM extensions in the [CiviCRM Sysadmin Guide](https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/).

## Getting Started

1. Configure the CMS permission "administer reserved option values".
1. Set `is_reserved=`1 in the database for either individual option values or the whole `civicrm_option_group.is_reserved`.

## Known Issues

* Many option groups are set to reserved by default. Might be ok though, just the purpose of this extension is to just restrict the handful where you've tied some automation or report functionality.
* TODO: Doesn't yet prevent using api or api explorer to make changes.
* TODO: inline editing, e.g. on the custom field admin screen, needs restricting.
* This doesn't restrict message templates, which is another area where sometimes you've set up the message template to do some fancy things, and then someone comes along and wipes your customization.
